from django import forms
from .models import Decisions


class DecisionForm(forms.ModelForm):
    class Meta:
        model = Decisions
        fields = ['text']
