from django.db import models


class Decisions(models.Model):
    question = models.TextField()
    answer = models.CharField(max_length=3,choices=[('ДА', 'ДА'), ('НЕТ', 'НЕТ')])
